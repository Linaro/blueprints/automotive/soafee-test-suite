# SOAFEE Test Suite - Deprecated

SOAFEE Test Suite has a new home; please use [SOAFEE Test Suite](https://gitlab.com/soafee/soafee-test-suite/)

SOAFEE compliance test to determine whether a software stack is SOAFEE compliant
or not.

## Installation

`soafee-test-suite` is a bash script and is self-contained with only awk as
dependency. It will work without installation, but it can be installed.

Install SOAFEE Test Suite:

```bash
make install
```

## Running SOAFEE Test Suite:

To run SOAFEE Test Suite:

```bash
soafee-test-suite run
```

`soafee-test-suite run` is a wrapper around the `bats` tool. Any options to
`run` will also be passed to `bats`. Hint: run `soafee-test-suite run -h` to get
`bats` help.

## SOAFEE Test Suite Usage and Examples

:

## TODO

* Add SOAFEE Test Suite Bitbake recipe

* Port all EWAOL tests to SOAFEE

* Add SOAFEE Test Suite Usage and Examples

* Remove meta-* after done with them

* Update the documentation according to SOAFEE Test Suite

## License

[MIT](https://choosealicense.com/licenses/mit/)

### SPDX Identifiers

Individual files contain the following tag instead of the full license text.

```text
  SPDX-License-Identifier: MIT
```

This enables machine processing of license information based on the SPDX
License Identifiers that are here available: http://spdx.org/licenses/
