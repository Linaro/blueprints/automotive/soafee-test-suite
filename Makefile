PREFIX:=/usr/local

.PHONY: install
install: soafee-test-suite tests/
	mkdir -p $(PREFIX)/share/soafee-test-suite
	@# Don't keep ownership to avoid host contamination in Bitbake
	cp -R --no-preserve=ownership bin tests $(PREFIX)/share/soafee-test-suite
	mkdir -p $(PREFIX)/bin
	@# Use relative symlink to avoid symlink-to-sysroot Bitbake warning
	cd $(PREFIX)/bin && \
		ln -s ../share/soafee-test-suite/bin/soafee-test-suite soafee-test-suite

.PHONY: uninstall
uninstall:
	rm -f $(PREFIX)/bin/soafee-test-suite
	rm -rf $(PREFIX)/share/soafee-test-suite
